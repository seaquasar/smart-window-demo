from flask import Response
import cv2 as cv2
from flask import Flask, request, render_template
import requests

# Declaring global variable for capturing geolocation and address
geolocation = {}
#address = ""

app = Flask(__name__)

camera = cv2.VideoCapture(0)  # use 0 for web camera


# for local webcam use cv2.VideoCapture(0)

def gen_frames():  # generate frame by frame from camera
    global geolocation, address
    while True:
        # Capture frame-by-frame
        success, frame = camera.read()  # read the camera frame
        if not success:
            break
        else:
            font = cv2.FONT_HERSHEY_PLAIN
            # org
            org = (10, 40)
            # fontScale
            fontScale = 1
            # Red color in BGR
            color = (0, 0, 255)
            # Line thickness of 2 px
            thickness = 1
            dt = str(geolocation)
            # flip the mirrored frame
            frame = cv2.flip(frame, 1)
            # put the dt variable over the frame
            frame = cv2.putText(frame, dt, org, font, fontScale,
                                color, thickness, cv2.LINE_AA, False)
            #dt = address
            #frame = cv2.putText(frame, dt, (10, 80), font, fontScale,
                               # color, thickness, cv2.LINE_AA, False)
            ret, buffer = cv2.imencode('.jpg', frame)
            # video frame
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result


@app.route('/video_feed')
def video_feed():
    # Video streaming route. Put this in the src attribute of an img tag
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/')
def webcam():
    """Video streaming home page."""
    return render_template('index.html')


@app.route('/getdata', methods=['POST'])
def getdata():
    global geolocation, address
    geolocation = request.get_json()
    loc = (str(geolocation['loc'])).split(", ")
    lat = float(loc[0])
    long = float(loc[1])
    # api-endpoint
    URL = "https://places.ls.hereapi.com/places/v1/discover/explore"

    # API key
    api_key = 'tCLOWQv2DtK02jWzK7lxRq933DNH8HrE7chf60cE_-c'
    r = 100
    # Defining a params dictionary for the parameters to be sent to the API
    PARAMS = {
        'at': '{},{}'.format(lat, long),
        'apikey': api_key,
        'cat' : "Landmark-Attraction",
        'r' : r,
    }

    # Sending get request and saving the response as response object
    r = requests.get(url=URL, params=PARAMS)

    # Extracting data in json format
    data = r.json()

    # Taking out title from JSON
    #address = data['items'][0]['title']

    print(data)
    print('\n')
    print('\n')
    print('\n')
    print('\n')
    return "Getting the json data for longitude and latitude"


if __name__ == '__main__':
    app.run(debug=True)
